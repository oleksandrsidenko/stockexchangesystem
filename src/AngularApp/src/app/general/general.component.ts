import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, Params } from '@angular/router'

@Component({
    selector: 'general',
    template: 
    `<h2>Stock Exchange</h2>
    <div>User: {{email}}</div>
    <account></account>
    <div class="radio-b">
        <label for="r1">Buy Sell Task</label>
        <input name="tasks" [(ngModel)]="tasks" type="radio" id="r1" value="BuySell">
        <label for="r2">Notification Task</label>
        <input name="tasks" [(ngModel)]="tasks" type="radio" id="r2" value="Message">
    </div>
    <buySell *ngIf="tasks === 'BuySell'"></buySell>
    <notify *ngIf="tasks === 'Message'"></notify>
    <h2>tasks list</h2>
    <tasksList></tasksList>`
})
export class GeneralComponent implements OnInit {
    private email: string
    private tasks: string

    constructor(private router: Router, private aRouter: ActivatedRoute) {}

    ngOnInit() {
        this.email = this.aRouter.snapshot.params['email']
    }
}