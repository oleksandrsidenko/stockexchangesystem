import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component'
import { CurrencyComponent } from './currency/currency.component'
import { UserComponent } from './user/user.component'
import { BaseTaskComponent } from './tasks/base-task.component'
import { BuySellComponent } from './tasks/buy-sell.component'
import { TasksListComponent } from './tasks/tasks-list.component'
import { AccountComponent } from './account/account.component'
import { GeneralComponent } from './general/general.component'
import { NotifyComponent } from './tasks/notify.component'

const routes: Routes = [
    { path: 'app', component: AppComponent, outlet: 'primary' },
    { path: 'currency', component: CurrencyComponent},
    { path: 'user', component: UserComponent, outlet: 'login'},
    { path: 'baseTask', component: BaseTaskComponent},
    { path: 'buySell', component: BuySellComponent},
    { path: 'notify', component: NotifyComponent },
    { path: 'taskList', component: TasksListComponent},
    { path: 'account', component: AccountComponent},
    { path: 'general/:email', component: GeneralComponent, outlet: 'main'}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}