import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { AppRoutingModule } from './app.routing.module' 

import { HttpService } from './services/http.service'

import { AppComponent } from './app.component'
import { CurrencyComponent } from './currency/currency.component'
import { UserComponent } from './user/user.component'
import { BaseTaskComponent } from './tasks/base-task.component'
import { BuySellComponent } from './tasks/buy-sell.component'
import { NotifyComponent } from './tasks/notify.component'
import { TasksListComponent } from './tasks/tasks-list.component'
import { AccountComponent } from './account/account.component'
import { GeneralComponent } from './general/general.component'

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, AppRoutingModule],
    declarations: [AppComponent, CurrencyComponent, UserComponent, AccountComponent,
        BaseTaskComponent, BuySellComponent, TasksListComponent, GeneralComponent,
        NotifyComponent],
    providers: [HttpService],
    bootstrap: [AppComponent]
})
export class AppModule {}