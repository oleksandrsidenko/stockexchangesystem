import { Component, EventEmitter, OnInit, Output } from '@angular/core'
import { HttpService } from '../services/http.service'

@Component({
    selector: 'currency',
    template: 
    `<div>
        <select>
            <option *ngFor="let curr of currencies" (click)=onSelect(curr)>
                {{curr}}
            </option>
        </select>
    </div>`
})
export class CurrencyComponent implements OnInit {
    private currencies: string[] = []

    @Output()
    setCurrency: EventEmitter<string> = new EventEmitter<string>()

    constructor(private httpService: HttpService) { }

    ngOnInit() {
        this.httpService.get('api/rate')
            .subscribe(data => this.currencies = data)
    }

    onSelect(selected: string) {
        this.setCurrency.emit(selected)
    }
}