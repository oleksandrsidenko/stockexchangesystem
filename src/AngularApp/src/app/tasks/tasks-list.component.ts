import { Component, OnInit, OnDestroy } from '@angular/core'
import { HttpService } from '../services/http.service'
import { BaseTask } from '../models/baseTask'
import 'rxjs/add/observable/interval'
import { Observable, Subscription } from "rxjs/Rx"

@Component({
    selector: 'tasksList',
    template: 
    `<div>
        <ul>
            <li *ngFor="let task of tasks" (click)=onSelect(task)>
                <p>{{task.Description}}</p>
            </li>
        </ul>
    </div>`
})
export class TasksListComponent implements OnInit, OnDestroy {
    private tasks: BaseTask[] = []
    private sub: Subscription

    constructor(private service: HttpService) { }

    ngOnInit() {
        this.sub = Observable.interval(5000)
            .subscribe(() => {                
                if (localStorage.getItem('user') !== null)
                    this.service.get(`api/tasks/${localStorage.getItem('user')}`)
                        .subscribe(data => this.tasks = data)
            })
    }

    ngOnDestroy() {
        this.sub.unsubscribe()
    }
}