import { Component, EventEmitter, Output, OnInit } from '@angular/core'
import { BaseTask } from '../models/baseTask'

@Component({
    selector: 'baseTask',
    template: 
    `<div>
       <label for="priority">Set task priority</label>
       <input type="text" id="priority" [(ngModel)]="task.Priority" required />
    </div>
    <div>
        <label for="description">Set task description</label>
        <input type="text" id="description" [(ngModel)]="task.Description" required />
    </div>
    <div>
        <label for="expiryDate">Set expiry date</label>
        <input type="text" id="expiryDate" [(ngModel)]="task.ExpiryDate" required />
    </div>
    <div>
        <label for="repeatable">Set repeatable</label>
        <input type="checkbox" id="repeatable" [(ngModel)]="task.IsRepeatable">
    </div>`
})
export class BaseTaskComponent implements OnInit {
    private task = new BaseTask()

    @Output()
    baseTask: EventEmitter<BaseTask> = new EventEmitter<BaseTask>()

    ngOnInit() {
        this.baseTask.emit(this.task)
    }
}