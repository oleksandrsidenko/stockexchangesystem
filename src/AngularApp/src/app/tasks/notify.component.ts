import { Component } from '@angular/core'
import { HttpService } from '../services/http.service'
import { Message } from '../models/message'

@Component({
    selector: 'notify',
    templateUrl: './task.html'    
})
export class NotifyComponent {
    private purchaseCurrency: string
    private saleCurrency: string
    private purchaseAmount: number
    private saleAmount: number
    private message: Message

    constructor(private service: HttpService) { }

    setPurchase(event: any) {
        this.purchaseCurrency = event
    }

    setSell(event: any) {
        this.saleCurrency = event
    }

    initiateTask(event: any) {
        this.message = event
    }

    createTask() {
        this.message.UserId = localStorage.getItem('user')
        this.message.CurrencyPurchase = this.purchaseCurrency
        this.message.Rate = this.saleAmount / this.purchaseAmount
        this.message.CurrencySale = this.saleCurrency
        this.message.TypeId = 2
        this.service.post('api/tasks/create/notify', JSON.stringify(this.message))
    }
}