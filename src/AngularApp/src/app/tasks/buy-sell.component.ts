import { Component } from '@angular/core'
import { HttpService } from '../services/http.service'
import { BuySell } from '../models/buySellTask'

@Component({
    selector: 'buySell',
    templateUrl: './task.html'
})
export class BuySellComponent {
    private purchaseCurrency: string
    private saleCurrency: string
    private purchaseAmount: number
    private saleAmount: number
    private buySellTask: BuySell

    constructor(private service: HttpService) { }

    setPurchase(event: any) {
        this.purchaseCurrency = event
    }

    setSell(event: any) {
        this.saleCurrency = event
    }

    initiateTask(event: any) {
        this.buySellTask = event
    }

    createTask() {
        this.buySellTask.UserId = localStorage.getItem('user')
        this.buySellTask.AmountPurchase = this.purchaseAmount
        this.buySellTask.CurrencyPurchase = this.purchaseCurrency
        this.buySellTask.AmountSale = this.saleAmount
        this.buySellTask.CurrencySale = this.saleCurrency
        this.buySellTask.TypeId = 1
        this.service.post('api/tasks/create/purchase', JSON.stringify(this.buySellTask))
    }
}