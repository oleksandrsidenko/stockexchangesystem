import { Component, EventEmitter, OnInit, Output } from '@angular/core'
import { HttpService } from '../services/http.service'
import { Account } from '../models/account'

@Component({
    selector: 'account',
    template:
    `<div>
        <label for="acc">Create/Replenish an account</label>
        <currency id="acc" (setCurrency)="setAccountCurrency($event)"></currency>
        <input type="text" [(ngModel)]="accountAmount" required />
        <button (click)="save()">Submit</button>
    </div>`
})
export class AccountComponent {
    private accountCurrency: string
    private accountAmount: number

    constructor(private service: HttpService) { }

    setAccountCurrency(event: any) {
        this.accountCurrency = event
    }

    save() {
        var account = new Account()
        account.Amount = this.accountAmount
        account.Currency = this.accountCurrency

        this.service.post(`api/account/replenish/${localStorage.getItem('user')}`, JSON.stringify(account))
    }
}