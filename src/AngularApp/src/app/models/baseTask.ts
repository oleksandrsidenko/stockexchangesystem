export class BaseTask {
    UserId: string
    Priority: number
    Description: string
    ExpiryDate: Date
    IsRepeatable: boolean
    TypeId: number
}