import { BaseTask } from './baseTask'

export class Message extends BaseTask {
    CurrencyPurchase: string
    CurrencySale: string
    Rate: number
}