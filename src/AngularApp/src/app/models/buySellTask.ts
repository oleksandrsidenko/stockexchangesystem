import { BaseTask } from './baseTask'

export class BuySell extends BaseTask {
    AmountPurchase: number
    CurrencyPurchase: string
    AmountSale: number
    CurrencySale: string
}