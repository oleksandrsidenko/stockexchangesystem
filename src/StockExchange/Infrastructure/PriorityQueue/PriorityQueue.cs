﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.PriorityQueue
{
    public class PriorityQueue<T> : IEnumerable<T> where T : class
    {
        private static readonly SortedDictionary<int, Queue<T>> _storage = new SortedDictionary<int, Queue<T>>();
        private readonly object _sync = new object();
        private int count;

        public PriorityQueue()
        {
            count = 0;
        }

        public bool IsEmpty()
        {
            return count == 0;
        }

        public void Enqueue(int priority, T item)
        {
            lock (_sync)
            {
                if (!_storage.ContainsKey(priority))
                    _storage.Add(priority, new Queue<T>());
                _storage[priority].Enqueue(item);
                count++;
            }
        }

        public T Dequeue()
        {
            lock (_sync)
            {
                if (!IsEmpty())
                {
                    T obj = _storage.First().Value.Dequeue();
                    count--;
                    if (_storage.First().Value.Count == 0)
                        _storage.Remove(_storage.First().Key);
                    return obj;
                }
            }
            return null;
        }

        public T Dequeue(int priority)
        {
            lock (_sync)
            {
                if (_storage.ContainsKey(priority))
                {
                    T obj = _storage[priority].Dequeue();
                    count--;
                    if (_storage[priority].Count == 0)
                        _storage.Remove(priority);
                    return obj;
                }
            }
            return null;
        }

        public T Peek()
        {
            lock (_sync)
                if (!IsEmpty())
                    return _storage.First().Value.Peek();
            return null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            while (!IsEmpty())
                yield return Dequeue();
        }
    }
}