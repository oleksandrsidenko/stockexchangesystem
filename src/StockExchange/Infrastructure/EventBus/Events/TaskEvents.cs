﻿using Infrastructure.EventBus.Jobs;
using System;

namespace Infrastructure.EventBus.Events
{
    public static class TaskEvents
    {
        public class Created
        {
            public readonly Job job;

            public Created(Job job)
            {
                this.job = job;
            }
        }

        public class Canceled
        {
            public readonly Guid taskId;

            public Canceled(Guid taskId)
            {
                this.taskId = taskId;
            }
        }
    }
}