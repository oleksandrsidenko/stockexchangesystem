﻿using BusinessLogicInterfaces;
using Entities;
using Infrastructure.EventBus.Events;
using System;
using System.Threading;

namespace Infrastructure.EventBus.Jobs
{
    public abstract class Job
    {
        public int Priority { get; private set; }
        protected readonly BaseTask _task;
        protected readonly IEventAggregator _aggregator;
        protected readonly IRateService _rateService;
        protected readonly ITaskService _taskService;
        private readonly Subscription<TaskEvents.Canceled> _canceledTask;
        private bool isCanceled;

        public Job(BaseTask task,
                   IEventAggregator aggregator,
                   IRateService rateService,
                   ITaskService taskService)
        {
            isCanceled = false;
            _aggregator = aggregator;
            _rateService = rateService;
            _taskService = taskService;
            _task = task;
            Priority = task.Priority;
            _canceledTask = aggregator.Subscribe<TaskEvents.Canceled>(CancelJob);
        }

        public void Execute()
        {
            if (_task.IsRepeatable)
                while (!isCanceled && _task.ExpiryDate != DateTime.Now && !DoJob())
                    Thread.Sleep(1000);
            else
                DoJob();
        }

        private void CancelJob(TaskEvents.Canceled canceledTask)
        {
            if (_task.Id.Equals(canceledTask.taskId))
                isCanceled = true;
        }

        public abstract bool DoJob();
    }
}