﻿using System;
using System.Reflection;

namespace Infrastructure.EventBus
{
    public class Subscription<TMessage> : IDisposable
    {
        public readonly MethodInfo MethodInfo;
        private readonly EventAggregator EventAggregator;
        public readonly WeakReference TargetObject;
        public readonly bool IsStatic;

        private bool isDisposed;

        public Subscription(Action<TMessage> action, EventAggregator eventAggregator)
        {
            MethodInfo = action.Method;
            if (action.Target == null)
                IsStatic = true;
            TargetObject = new WeakReference(action.Target);
            EventAggregator = eventAggregator;
        }

        ~Subscription()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            EventAggregator.UnSubscribe(this);
            isDisposed = true;
        }

        public Action<TMessage> CreateAction()
        {
            if (TargetObject.Target != null && TargetObject.IsAlive)
                return (Action<TMessage>)Delegate.CreateDelegate(typeof(Action<TMessage>), TargetObject.Target, MethodInfo);
            if (this.IsStatic)
                return (Action<TMessage>)Delegate.CreateDelegate(typeof(Action<TMessage>), MethodInfo);

            return null;
        }
    }
}