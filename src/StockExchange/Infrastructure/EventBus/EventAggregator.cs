﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.EventBus
{
    public interface IEventAggregator
    {
        void Publish<TMessageType>(TMessageType message);
        Subscription<TMessageType> Subscribe<TMessageType>(Action<TMessageType> action);
        void UnSubscribe<TMessageType>(Subscription<TMessageType> subscription);
    }

    public class EventAggregator : IEventAggregator
    {
        private readonly object _sync = new object();
        private Dictionary<Type, IList> subscribers;

        public EventAggregator()
        {
            subscribers = new Dictionary<Type, IList>();
        }

        public void Publish<TMessageType>(TMessageType message)
        {
            var t = typeof(TMessageType);
            IList subList;
            if (subscribers.ContainsKey(t))
            {
                lock (_sync)
                    subList = new List<Subscription<TMessageType>>(subscribers[t].Cast<Subscription<TMessageType>>());

                foreach (Subscription<TMessageType> sub in subList)
                    sub.CreateAction()?.Invoke(message);
            }
        }

        public Subscription<TMessageType> Subscribe<TMessageType>(Action<TMessageType> action)
        {
            var t = typeof(TMessageType);
            var actionDetail = new Subscription<TMessageType>(action, this);

            lock (_sync)
                if (!subscribers.TryGetValue(t, out IList actionList))
                {
                    actionList = new List<Subscription<TMessageType>>
                    {
                        actionDetail
                    };
                    subscribers.Add(t, actionList);
                }
                else
                    actionList.Add(actionDetail);

            return actionDetail;
        }

        public void UnSubscribe<TMessageType>(Subscription<TMessageType> subscription)
        {
            var t = typeof(TMessageType);
            if (subscribers.ContainsKey(t))
            {
                lock (_sync)
                    subscribers[t].Remove(subscription);
                subscription = null;
            }
        }
    }
}