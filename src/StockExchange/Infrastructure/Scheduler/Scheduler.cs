﻿using Infrastructure.EventBus.Events;
using Infrastructure.EventBus;
using Infrastructure.PriorityQueue;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Scheduler
{
    public class Scheduler
    {
        private readonly PriorityQueue<Task> _queue = new PriorityQueue<Task>();
        private readonly Thread _mainThread = null;

        public Scheduler(IEventAggregator aggregator)
        {
            aggregator.Subscribe<TaskEvents.Created>(this.QueueTask);
            _mainThread = new Thread(new ThreadStart(Execute));
            if (!_mainThread.IsAlive)
                _mainThread.Start();
        }

        private void Execute()
        {
            while (true)
            {
                var task = _queue.Dequeue();
                if (task != null)
                    task.Start();
            }
        }

        private void QueueTask(TaskEvents.Created e)
        {
            _queue.Enqueue(e.job.Priority, new Task(() => e.job.Execute()));
        }
    }
}