﻿using BusinessLogicInterfaces;
using Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Infrastructure.RateProcessor
{
    public class JsonProcessor : ICurrencyProcessor
    {
        public List<Currency> Parse(string path)
        {
            return JsonConvert.DeserializeObject<List<Currency>>(File.ReadAllText(path));
        }
    }
}