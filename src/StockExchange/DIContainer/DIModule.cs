﻿using Infrastructure.EventBus;
using BusinessLogicInterfaces;
using SimpleStockLogicImpl.Jobs;
using SimpleStockLogicImpl.Services;
using DataAccessInterfaces;
using EfDataAccessImpl.Context;
using EfDataAccessImpl.Repositories;
using Ninject.Modules;

namespace DIContainer
{
    public class DIModule : NinjectModule
    {
        private readonly string _failoverDb;
        private readonly string _centralStorage;
        private readonly string _currencyStoragePath;

        public DIModule(string failoverDb, string centralStorage, string currencyStoragePath)
        {
            _failoverDb = failoverDb;
            _centralStorage = centralStorage;
            _currencyStoragePath = currencyStoragePath;
        }

        public override void Load()
        {
            Bind<IAccountService>().To<AccountService>();
            Bind<INotificationService>().To<NotificationService>();
            Bind<IOrderService>().To<OrderService>();
            Bind<IRateService>().To<RateService>()
                .WithConstructorArgument("path", _currencyStoragePath);
            Bind<ITaskService>().To<TaskService>();
            Bind<IUserService>().To<UserService>();

            Bind<IEventAggregator>().To<EventAggregator>().InSingletonScope();
            Bind<JobsFactory>().ToSelf();

            Bind<IRepository>().To<GenericRepository>();
            Bind<ContextFactory>().ToSelf();            
            Bind<DatabaseContext>().ToSelf().WithConstructorArgument(_centralStorage);
            Bind<FailoverDbContext>().ToSelf().WithConstructorArgument(_failoverDb);
        }
    }
}