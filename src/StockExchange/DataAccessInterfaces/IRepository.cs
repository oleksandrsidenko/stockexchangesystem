﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DataAccessInterfaces
{
    public interface IRepository
    {
        IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class;

        IEnumerable<TEntity> GetAll<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;

        TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;
        TEntity Create<TEntity>(TEntity item) where TEntity : class;
        TEntity Update<TEntity>(TEntity item) where TEntity : class;
        TEntity Delete<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;
    }
}