﻿using Entities;
using System.Collections.Generic;

namespace BusinessLogicInterfaces
{
    public interface ICurrencyProcessor
    {
        List<Currency> Parse(string path);
    }
}