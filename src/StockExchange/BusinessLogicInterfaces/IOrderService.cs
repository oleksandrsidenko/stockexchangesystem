﻿using Entities;

namespace BusinessLogicInterfaces
{
    public interface IOrderService
    {
        void CreatePurchaseOrder(Order order);
    }
}