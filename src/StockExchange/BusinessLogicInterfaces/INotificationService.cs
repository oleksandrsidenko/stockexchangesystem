﻿using System;

namespace BusinessLogicInterfaces
{
    public interface INotificationService
    {
        void SendMessage(Guid userId, string subject, string body);
    }
}