﻿using Entities;
using System;
using System.Collections.Generic;

namespace BusinessLogicInterfaces
{
    public interface ITaskService
    {
        void CreateTask(BaseTask task);
        void ChangeTaskStatus(BaseTask task, int statusId);
        IEnumerable<BaseTask> GetUserTasks(Guid userId);
        void CancelTask(Guid taskId);
        void PublishTasks();
    }
}