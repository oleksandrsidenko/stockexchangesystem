﻿using Entities;
using System;

namespace BusinessLogicInterfaces
{
    public interface IUserService
    {
        User GetUserById(Guid userId);
        User GetUserByEmail(string email);
    }
}