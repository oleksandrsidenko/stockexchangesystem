﻿using Entities;
using System;
using System.Collections.Generic;

namespace BusinessLogicInterfaces
{
    public interface IAccountService
    {
        bool CheckSumOnAccount(Guid userId, string currency, decimal amount);
        void Purchase(Order order);
        IEnumerable<Account> GetUserAccounts(Guid userId);
        Account ReplenishAccount(Guid userId, decimal amount, string currency);
    }
}