﻿using System.Collections.Generic;

namespace BusinessLogicInterfaces
{
    public interface IRateService
    {
        double GetCurrentRate(string currencyPurchase, string currencySale);
        List<string> GetCurrencyList();
    }
}