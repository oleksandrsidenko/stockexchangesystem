﻿using EfDataAccessImpl.Configuration;
using Entities;
using System.Data.Entity;

namespace EfDataAccessImpl.Context
{
    public class DatabaseContext : DbContext
    {
        public IDbSet<User> Users { get; set; }
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<Order> Orders { get; set; }

        public DatabaseContext(string connection) : base(connection) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new UserConfig());
            modelBuilder.Configurations.Add(new AccountConfig());
            modelBuilder.Configurations.Add(new OrderConfig());
        }
    }
}