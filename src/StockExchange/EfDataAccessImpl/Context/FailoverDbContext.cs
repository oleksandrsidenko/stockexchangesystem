﻿using EfDataAccessImpl.Configuration;
using Entities;
using System.Data.Entity;

namespace EfDataAccessImpl.Context
{
    public class FailoverDbContext : DbContext
    {
        public IDbSet<Status> Statuses { get; set; }
        public IDbSet<BaseTask> BaseTasks { get; set; }
        public IDbSet<BuySellTask> BuySellTasks { get; set; }
        public IDbSet<MessageTask> MessageTasks { get; set; }
        public IDbSet<Type> Types { get; set; }

        public FailoverDbContext(string connection) : base(connection) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new BaseTaskConfig());
            modelBuilder.Configurations.Add(new BuySellTaskConfig());
            modelBuilder.Configurations.Add(new MessageTaskConfig());
            modelBuilder.Configurations.Add(new TypeConfig());
            modelBuilder.Configurations.Add(new StatusConfig());
        }
    }
}