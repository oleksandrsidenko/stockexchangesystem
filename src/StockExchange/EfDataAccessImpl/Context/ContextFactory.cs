﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace EfDataAccessImpl.Context
{
    public class ContextFactory : IDisposable
    {
        private readonly List<DbContext> _contexts;

        public ContextFactory(DatabaseContext databaseContext,
                              FailoverDbContext failoverDbContext)
        {
            _contexts = new List<DbContext>()
            {
                databaseContext,
                failoverDbContext
            };
        }

        public DbContext GetContext<TEntity>()
        {
            var entityName = typeof(TEntity).Name;
            foreach (var context in _contexts)            
                if (IsDbSetAttached(context, entityName))
                    return context;
            throw new Exception($"There are no context with {entityName} entity in DbSet");
        }

        private bool IsDbSetAttached(DbContext db, string entityName)
        {
            var workspace = ((IObjectContextAdapter)db).ObjectContext.MetadataWorkspace;            
            return workspace.GetItems<EntityType>(DataSpace.CSpace).Any(e => e.Name == entityName);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)                
                    _contexts.ForEach(x => x.Dispose());                
                disposed = true;
            }
        }
    }
}