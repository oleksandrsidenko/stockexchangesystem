﻿using DataAccessInterfaces;
using EfDataAccessImpl.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace EfDataAccessImpl.Repositories
{
    public class GenericRepository : IRepository
    {
        private readonly ContextFactory _factory;

        public GenericRepository(ContextFactory factory)
        {
            _factory = factory;            
        }

        public TEntity Create<TEntity>(TEntity item) where TEntity : class
        {
            var db = _factory.GetContext<TEntity>();
            db.Set<TEntity>().Add(item);
            db.SaveChanges();
            return item;
        }

        public TEntity Delete<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            var db = _factory.GetContext<TEntity>();
            var dbSet = db.Set<TEntity>();

            var item = dbSet.Where(predicate).FirstOrDefault();
            dbSet.Remove(item);
            db.SaveChanges();
            return item;
        }

        public TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            var db = _factory.GetContext<TEntity>();            
            return db.Set<TEntity>().Where(predicate).FirstOrDefault();
        }

        public IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return _factory.GetContext<TEntity>().Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return _factory.GetContext<TEntity>().Set<TEntity>().Where(predicate);
        }

        public TEntity Update<TEntity>(TEntity item) where TEntity : class
        {
            var db = _factory.GetContext<TEntity>();
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            return item;
        }
    }
}