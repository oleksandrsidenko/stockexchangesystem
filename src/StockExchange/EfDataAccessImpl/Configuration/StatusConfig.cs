﻿using Entities;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class StatusConfig : EntityTypeConfiguration<Status>
    {
        public StatusConfig()
        {
            this.HasKey<int>(x => x.Id);

            this.Property(x => x.Name)
                .HasMaxLength(11)
                .IsRequired();
        }
    }
}