﻿using Entities;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class MessageTaskConfig : EntityTypeConfiguration<MessageTask>
    {
        public MessageTaskConfig()
        {
            this.ToTable("MessageTasks");

            this.Property(x => x.CurrencyPurchase)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.CurrencySale)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.Rate)
                .HasColumnType("money")
                .IsRequired();
        }
    }
}