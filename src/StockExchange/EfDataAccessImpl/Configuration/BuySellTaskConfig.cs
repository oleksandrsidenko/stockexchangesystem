﻿using Entities;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class BuySellTaskConfig : EntityTypeConfiguration<BuySellTask>
    {
        public BuySellTaskConfig()
        {
            this.ToTable("BuySellTasks");

            this.Property(x => x.AmountPurchase)
                .HasColumnType("money")
                .IsRequired();

            this.Property(x => x.AmountSale)
                .HasColumnType("money")
                .IsRequired();

            this.Property(x => x.CurrencyPurchase)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.CurrencySale)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.Rate)
                .HasColumnType("money")
                .IsOptional();
        }
    }
}