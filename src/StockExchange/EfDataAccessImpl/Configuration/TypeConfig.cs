﻿using Entities;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class TypeConfig : EntityTypeConfiguration<Type>
    {
        public TypeConfig()
        {
            this.ToTable("Types");

            this.HasKey<int>(x => x.Id);

            this.Property(x => x.Name)
                .HasMaxLength(8)
                .IsRequired();
        }
    }
}