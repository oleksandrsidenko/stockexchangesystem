﻿using Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class BaseTaskConfig : EntityTypeConfiguration<BaseTask>
    {
        public BaseTaskConfig()
        {
            this.ToTable("BaseTasks");

            this.HasKey<Guid>(x => x.Id);

            this.Property(x => x.Description)
                .HasMaxLength(50)
                .IsRequired();

            this.Property(x => x.ExpiryDate)
                .HasColumnType("datetime")
                .IsOptional();

            this.Property(x => x.IsRepeatable)
                .HasColumnType("bit")
                .IsRequired();

            this.Property(x => x.Priority)
                .IsRequired();

            this.Property(x => x.StatusId)
                .IsRequired();

            this.Property(x => x.TimeStamp)
                .HasColumnType("datetime")
                .IsRequired();

            this.Property(x => x.TypeId)
                .IsRequired();

            this.Property(x => x.UserId)
                .HasColumnType("uniqueidentifier")
                .IsRequired();
        }
    }
}