﻿using Entities;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class OrderConfig : EntityTypeConfiguration<Order>
    {
        public OrderConfig()
        {
            this.ToTable("Orders");

            this.HasKey<int>(x => x.Id);

            this.Property(x => x.AmountPurchase)
                .HasColumnType("money")
                .IsRequired();

            this.Property(x => x.AmountSale)
                .HasColumnType("money")
                .IsRequired();

            this.Property(x => x.CurrencyPurchase)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.CurrencySale)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.UserId)
                .IsRequired();
        }
    }
}