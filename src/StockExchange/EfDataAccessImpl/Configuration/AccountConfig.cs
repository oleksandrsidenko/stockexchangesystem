﻿using Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class AccountConfig : EntityTypeConfiguration<Account>
    {
        public AccountConfig()
        {
            this.ToTable("Accounts");

            this.HasKey<Guid>(x => x.Id);

            this.Property(x => x.Amount)
                .HasColumnType("money")
                .IsRequired();

            this.Property(x => x.Currency)
                .HasMaxLength(3)
                .IsRequired();

            this.Property(x => x.UserId)
                .IsRequired();
        }
    }
}