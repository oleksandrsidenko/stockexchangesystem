﻿using Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace EfDataAccessImpl.Configuration
{
    public class UserConfig : EntityTypeConfiguration<User>
    {
        public UserConfig()
        {
            this.ToTable("Users");

            this.HasKey<Guid>(x => x.Id);

            this.Property(x => x.Email)
                .HasMaxLength(50)
                .IsRequired();

            this.HasMany<Account>(x => x.Accounts)
                .WithRequired(x => x.User);

            this.HasMany<Order>(x => x.Orders)
                .WithRequired(x => x.User);
        }
    }
}