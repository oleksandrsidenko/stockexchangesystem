﻿namespace Entities
{
    public class ReplenishAccount
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}