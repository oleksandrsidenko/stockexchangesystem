﻿using System;

namespace Entities
{
    public class BaseTask
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int Priority { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool IsRepeatable { get; set; }
    }
}