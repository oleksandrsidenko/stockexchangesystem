﻿using System;
using System.Collections.Generic;

namespace Entities
{
    public class User
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public ICollection<Account> Accounts { get; set; }
        public ICollection<Order> Orders { get; set; }

        public User()
        {
            Accounts = new List<Account>();
            Orders = new List<Order>();
        }
    }
}