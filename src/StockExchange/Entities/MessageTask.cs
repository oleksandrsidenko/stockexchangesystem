﻿namespace Entities
{
    public class MessageTask : BaseTask
    {
        public string CurrencyPurchase { get; set; }
        public string CurrencySale { get; set; }
        public decimal Rate { get; set; }
    }
}