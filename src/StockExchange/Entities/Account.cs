﻿using System;

namespace Entities
{
    public class Account
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}