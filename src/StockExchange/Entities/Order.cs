﻿using System;

namespace Entities
{
    public class Order
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public decimal AmountPurchase { get; set; }
        public string CurrencyPurchase { get; set; }
        public decimal AmountSale { get; set; }
        public string CurrencySale { get; set; }
        public User User { get; set; }
    }
}