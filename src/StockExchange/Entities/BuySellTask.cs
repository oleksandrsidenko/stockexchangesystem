﻿namespace Entities
{
    public class BuySellTask : BaseTask
    {
        public decimal AmountPurchase { get; set; }
        public string CurrencyPurchase { get; set; }
        public decimal AmountSale { get; set; }
        public string CurrencySale { get; set; }
        public decimal? Rate { get; set; }
    }
}