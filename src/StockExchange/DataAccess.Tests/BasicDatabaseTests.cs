﻿using DataAccessInterfaces;
using DIContainer;
using Entities;
using Ninject;
using NUnit.Framework;
using System;
using System.Configuration;

namespace DataAccess.Tests
{
    [TestFixture]
    public class BasicDatabaseTests
    {
        private string _dbConnectionString;
        private string _failoverDbConnectionString;
        private IRepository db;

        [SetUp]
        public void Initialize()
        {            
            _dbConnectionString = ConfigurationManager.ConnectionStrings[1].ConnectionString;
            _failoverDbConnectionString = ConfigurationManager.ConnectionStrings[2].ConnectionString;
            var kernel = new StandardKernel(new DIModule(_failoverDbConnectionString, _dbConnectionString, ""));
            db = kernel.Get<IRepository>();
        }

        [Test]
        public void ShouldCreateDbWithPredifinedValues()
        {
            var user = new User()
            {
                Id = Guid.NewGuid(),
                Email = "email"
            };

            var account = new Account()
            {
                Id = Guid.NewGuid(),
                Amount = 100,
                Currency = "USD",
                User = user,
                UserId = user.Id
            };
            user.Accounts.Add(account);

            var order = new Order()
            {
                Id = 1,
                AmountPurchase = 50,
                AmountSale = 50,
                CurrencyPurchase = "EUR",
                CurrencySale = "USD",
                User = user,
                UserId = user.Id
            };
            user.Orders.Add(order);

            Assert.DoesNotThrow(() =>
            {
                db.Create(user);
            });
        }

        [Test]
        public void ShouldCreateFailoverDbWithPredifinedValues()
        {
            var buySellTask = new BuySellTask()
            {
                Id = Guid.NewGuid(),
                Description = "Buy sell task",
                UserId = Guid.NewGuid(),
                ExpiryDate = DateTime.Now.AddDays(1),
                StatusId = 1,
                TimeStamp = DateTime.Now,
                TypeId = 1,
                AmountPurchase = 100,
                CurrencyPurchase = "USD",
                AmountSale = 130,
                CurrencySale = "EUR",
                IsRepeatable = true,
                Priority = 5                
            };

            var notifyTask = new MessageTask()
            {
                Id = Guid.NewGuid(),
                Description = "Message task",
                UserId = Guid.NewGuid(),
                StatusId = 2,
                TimeStamp = DateTime.Now,
                TypeId = 2,
                CurrencyPurchase = "USD",
                CurrencySale = "EUR",                
                Rate = 1.3M,
                IsRepeatable = false,
                Priority = 1                
            };

            Assert.DoesNotThrow(() =>
            {
                db.Create(buySellTask);
                db.Create(notifyTask);                
            });
        }
    }
}
