﻿using BusinessLogicInterfaces;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SimpleStockWebApi.Controllers
{
    [RoutePrefix("api/rate")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class СurrencyController : ApiController
    {
        public readonly IRateService _service;

        public СurrencyController(IRateService service)
        {
            _service = service;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Json(_service.GetCurrencyList());
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}