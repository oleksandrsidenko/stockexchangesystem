﻿using BusinessLogicInterfaces;
using Entities;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SimpleStockWebApi.Controllers
{
    [RoutePrefix("api/account")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountController : ApiController
    {
        private readonly IAccountService _service;

        public AccountController(IAccountService service)
        {
            _service = service;
        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                return Json(_service.GetUserAccounts(Guid.Parse(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("replenish/{id}")]
        [HttpPost]
        public IHttpActionResult Replenish(string id, [FromBody] ReplenishAccount acc)
        {
            try
            {
                _service.ReplenishAccount(Guid.Parse(id), acc.Amount, acc.Currency);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}