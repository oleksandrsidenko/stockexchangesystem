﻿using BusinessLogicInterfaces;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SimpleStockWebApi.Controllers
{
    [RoutePrefix("api/users")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [Route("{email}")]
        [HttpGet]
        public IHttpActionResult ByEmail(string email)
        {
            try
            {
                return Json(_service.GetUserByEmail(email));
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}