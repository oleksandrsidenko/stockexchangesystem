﻿using BusinessLogicInterfaces;
using Entities;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SimpleStockWebApi.Controllers
{
    [RoutePrefix("api/tasks")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaskController : ApiController
    {
        private readonly ITaskService _service;

        public TaskController(ITaskService service)
        {
            _service = service;
        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            try
            {
                return Json(_service.GetUserTasks(Guid.Parse(id)));
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("create/purchase")]
        [HttpPost]
        public IHttpActionResult CreatePurchaseTask([FromBody] BuySellTask task)
        {
            try
            {
                _service.CreateTask(task);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("create/notify")]
        [HttpPost]
        public IHttpActionResult CreateNotifyTask([FromBody] MessageTask task)
        {
            try
            {
                _service.CreateTask(task);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("cancel/{id}")]
        [HttpPost]
        public IHttpActionResult CancelTask(string id)
        {
            try
            {
                _service.CancelTask(Guid.Parse(id));
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}