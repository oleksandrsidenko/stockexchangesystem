﻿using BusinessLogicInterfaces;
using Entities;
using Infrastructure.EventBus;
using Infrastructure.EventBus.Jobs;
using System;

namespace SimpleStockLogicImpl.Jobs
{
    public class MessageJob : Job
    {
        private readonly INotificationService _notificationService;
        public MessageJob(MessageTask task,
                          IEventAggregator aggregator,
                          INotificationService notificationService,
                          IRateService rateService,
                          ITaskService taskService)
            : base(task, aggregator, rateService, taskService)
        {
            _notificationService = notificationService;
        }

        public override bool DoJob()
        {
            var messageTask = (MessageTask)_task;
            var currentRate = messageTask.Rate * Convert.ToDecimal(_rateService
                .GetCurrentRate(messageTask.CurrencyPurchase, messageTask.CurrencySale));

            if (messageTask.Rate >= currentRate)
            {
                var body = $"The current exchange rate has reached {currentRate}";
                _notificationService.SendMessage(_task.UserId, "Currency rate notification", body);

                _task.StatusId = 2;
                _taskService.ChangeTaskStatus(_task, _task.StatusId);
                return true;
            }
            return false;
        }
    }
}