﻿using BusinessLogicInterfaces;
using Entities;
using Infrastructure.EventBus;
using Infrastructure.EventBus.Jobs;
using System;

namespace SimpleStockLogicImpl.Jobs
{
    public class JobsFactory
    {
        private readonly IAccountService _accountService;
        private readonly IEventAggregator _aggregator;
        private readonly INotificationService _notificationService;
        private readonly IRateService _rateService;

        public JobsFactory(IAccountService accountService,
                           IEventAggregator aggregator,
                           INotificationService notificationService,
                           IRateService rateService)
        {
            _accountService = accountService;
            _aggregator = aggregator;
            _notificationService = notificationService;
            _rateService = rateService;
        }

        public Job CreateJob(BaseTask task, ITaskService service)
        {
            switch (task.TypeId)
            {
                case 1:
                    return new BuySellJob((BuySellTask)task,
                                          _accountService,
                                          _aggregator,
                                          _rateService,
                                          service);
                case 2:
                    return new MessageJob((MessageTask)task,
                                          _aggregator,
                                          _notificationService,
                                          _rateService,
                                          service);
                default:
                    throw new Exception($"Jobs type {task.TypeId} is not supported");
            }
        }
    }
}