﻿using BusinessLogicInterfaces;
using Entities;
using Infrastructure.EventBus;
using Infrastructure.EventBus.Jobs;
using System;

namespace SimpleStockLogicImpl.Jobs
{
    public class BuySellJob : Job
    {
        private readonly IAccountService _accountService;
        public BuySellJob(BuySellTask task,
                          IAccountService accountService,
                          IEventAggregator aggregator,
                          IRateService rateService,
                          ITaskService taskService)
            : base(task, aggregator, rateService, taskService)
        {
            _accountService = accountService;
        }

        public override bool DoJob()
        {
            var buySellTask = (BuySellTask)_task;            
            var rate = buySellTask.AmountSale / buySellTask.AmountPurchase;
            var currentRate = rate * Convert.ToDecimal(_rateService.GetCurrentRate(buySellTask.CurrencyPurchase, buySellTask.CurrencySale));

            if (rate >= currentRate)
            {
                if (!_accountService.CheckSumOnAccount(_task.UserId, buySellTask.CurrencySale, currentRate * buySellTask.AmountPurchase))
                    _task.StatusId = 3;
                else
                {
                    var order = new Order()
                    {
                        AmountPurchase = buySellTask.AmountPurchase,
                        AmountSale = buySellTask.AmountPurchase * currentRate,
                        CurrencyPurchase = buySellTask.CurrencyPurchase,
                        CurrencySale = buySellTask.CurrencySale,                        
                        UserId = buySellTask.UserId
                    };

                    _accountService.Purchase(order);
                    _task.StatusId = 2;
                }
                _taskService.ChangeTaskStatus(_task, _task.StatusId);
                return true;
            }
            return false;
        }
    }
}