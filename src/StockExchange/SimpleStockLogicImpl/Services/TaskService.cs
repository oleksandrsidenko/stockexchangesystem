﻿using Infrastructure.EventBus;
using Infrastructure.EventBus.Events;
using BusinessLogicInterfaces;
using SimpleStockLogicImpl.Jobs;
using DataAccessInterfaces;
using Entities;
using System;
using System.Collections.Generic;

namespace SimpleStockLogicImpl.Services
{
    public class TaskService : ITaskService
    {
        private readonly IEventAggregator _aggregator;
        private readonly JobsFactory _jobsFactory;
        private readonly INotificationService _notificationService;
        private readonly IRepository _db;

        public TaskService(
            IEventAggregator aggregator,
            JobsFactory jobsFactory,
            INotificationService notificationService,
            IRepository db)
        {
            _aggregator = aggregator;
            _jobsFactory = jobsFactory;
            _notificationService = notificationService;
            _db = db;
        }

        public void CreateTask(BaseTask task)
        {
            task.TimeStamp = DateTime.Now;
            task.StatusId = 1;
            task.Id = Guid.NewGuid();
            CreateJob(_db.Create(task));
        }

        private void CreateJob(BaseTask task)
        {
            _aggregator.Publish(new TaskEvents.Created(_jobsFactory.CreateJob(task, this)));
        }

        public IEnumerable<BaseTask> GetUserTasks(Guid userId)
        {
            return _db.GetAll<BaseTask>(x => x.UserId.Equals(userId) && x.StatusId == 1);
        }

        public void CancelTask(Guid taskId)
        {
            _aggregator.Publish(new TaskEvents.Canceled(taskId));
        }

        public void ChangeTaskStatus(BaseTask task, int statusId)
        {
            task.StatusId = statusId;
            _db.Delete<BaseTask>(x => x.Id.Equals(task.Id));

            var subject = "Task notification";
            var body = $"Your task {task.Description} was completed with status {_db.Get<Status>(x => x.Id == task.StatusId).Name}";
            _notificationService.SendMessage(task.UserId, subject, body);
        }

        public void PublishTasks()
        {
            foreach (var task in _db.GetAll<BaseTask>())
                CreateJob(task);
        }
    }
}