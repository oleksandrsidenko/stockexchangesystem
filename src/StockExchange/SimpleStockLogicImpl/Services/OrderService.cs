﻿using BusinessLogicInterfaces;
using DataAccessInterfaces;
using Entities;

namespace SimpleStockLogicImpl.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository _db;

        public OrderService(IRepository db)
        {
            _db = db;
        }

        public void CreatePurchaseOrder(Order order)
        {
            _db.Create(order);
        }
    }
}