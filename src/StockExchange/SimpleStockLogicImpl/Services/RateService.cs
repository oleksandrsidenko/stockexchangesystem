﻿using BusinessLogicInterfaces;
using Entities;
using Infrastructure.RateProcessor;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleStockLogicImpl.Services
{
    public class RateService : IRateService
    {
        public Func<ICurrencyProcessor> Processor { get; set; }
        private readonly List<Currency> _currencyStorage;

        public RateService(string path)
        {
            Processor = () => new JsonProcessor();
            _currencyStorage = Processor().Parse(path);
        }

        public List<string> GetCurrencyList()
        {
            return _currencyStorage.Select(x => x.Name).ToList();
        }

        public double GetCurrentRate(string currencyPurchase, string currencySale)
        {
            return new Random().NextDouble() * (1.1 - 0.9) + 0.9;
        }
    }
}