﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicInterfaces;
using DataAccessInterfaces;

namespace SimpleStockLogicImpl.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRepository _db;
        private readonly INotificationService _notificationService;
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public AccountService(IRepository db,
                              INotificationService notificationService,
                              IOrderService orderService,
                              IUserService userService)
        {
            _notificationService = notificationService;
            _orderService = orderService;
            _userService = userService;
            _db = db;
        }

        private Account CreateAccount(Guid userId, decimal amount, string currency)
        {
            var account = _db.Get<Account>(x => x.UserId.Equals(userId) && x.Currency.Equals(currency));
            if (account == null)
            {
                account = new Account()
                {
                    Amount = amount,
                    Currency = currency,
                    Id = Guid.NewGuid(),
                    UserId = userId
                };
                return _db.Create(account);
            }
            return account;
        }

        public IEnumerable<Account> GetUserAccounts(Guid userId)
        {
            IEnumerable<Account> accounts = _db.GetAll<Account>(x => x.UserId.Equals(userId));
            if (accounts.Count() == 0)
                throw new Exception($"User {userId} does not have accounts");
            return accounts;
        }

        public Account ReplenishAccount(Guid userId, decimal amount, string currency)
        {
            var account = GetAccount(userId, currency);
            if (amount > 0)
            {
                account.Amount += amount;
                return _db.Update(account);
            }
            return account;
        }

        public bool CheckSumOnAccount(Guid userId, string currency, decimal amount)
        {
            var account = GetAccount(userId, currency);
            return CheckSumOnAccount(account, amount);
        }

        public void Purchase(Order order)
        {
            var accountSale = GetAccount(order.UserId, order.CurrencySale);
            if (CheckSumOnAccount(accountSale, order.AmountSale))
            {
                var accountPurchase = GetAccount(order.UserId, order.CurrencyPurchase);
                accountSale.Amount -= order.AmountSale;
                accountPurchase.Amount += order.AmountPurchase;
                _db.Update(accountSale);
                _db.Update(accountPurchase);
                _orderService.CreatePurchaseOrder(order);

                var body = $"Successfully purchased {order.AmountPurchase}{order.CurrencyPurchase}. " +
                    $"{order.AmountSale}{order.CurrencyPurchase} was withdrawn from the account";
            }
            else
            {
                var body = $"It was an attempt to make a purchase {order.AmountPurchase}{order.CurrencyPurchase} but did not have enough money";
                SendPurchaseNotification(order.UserId, body);
                throw new Exception($"User: {order.UserId}, does not have enough money to made a purchase");
            }
        }

        private bool CheckSumOnAccount(Account account, decimal amount)
        {
            return account.Amount >= amount;
        }

        private Account GetAccount(Guid userId, string currency)
        {
            var account = _db.Get<Account>(x => x.UserId.Equals(userId) && x.Currency.Equals(currency));
            if (account == null)
                return CreateAccount(userId, 0, currency);
            return account;
        }

        private void SendPurchaseNotification(Guid userId, string body)
        {
            var subject = "Purchase notification";
            _notificationService.SendMessage(userId, subject, body);
        }
    }
}