﻿using BusinessLogicInterfaces;
using DataAccessInterfaces;
using Entities;
using System;
using System.Net;
using System.Net.Mail;

namespace SimpleStockLogicImpl.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IRepository _db;

        public NotificationService(IRepository db)
        {
            _db = db;
        }

        public void SendMessage(Guid userId, string subject, string body)
        {
            var user = _db.Get<User>(u => u.Id.Equals(userId));
            if (user != null)
                SendMessage(user.Email, subject, body);
            else
                throw new Exception($"There are no user with id {userId}");
        }

        private void SendMessage(string email, string subject, string body)
        {
            /*var fromAddress = new MailAddress("admin@noreply.com", "admin@noreply.com");
            var toAddress = new MailAddress(email, email);

            var smtp = new SmtpClient()
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, "fromPassword")
            };

            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }*/
        }
    }
}