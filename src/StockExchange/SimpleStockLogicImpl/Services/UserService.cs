﻿using BusinessLogicInterfaces;
using DataAccessInterfaces;
using Entities;
using System;

namespace SimpleStockLogicImpl.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository _db;
        public UserService(IRepository db)
        {
            _db = db;
        }

        private User CreateUser(string email)
        {
            return _db.Create(new User() { Id = Guid.NewGuid(), Email = email }); ;
        }

        public User GetUserById(Guid userId)
        {
            var user = _db.Get<User>(x => x.Id.Equals(userId));
            if (user == null)
                throw new Exception($"User with id:{userId} not found");
            return user;
        }

        public User GetUserByEmail(string email)
        {
            var user = _db.Get<User>(x => x.Email.Equals(email));
            if (user == null)
                return CreateUser(email);
            return user;
        }
    }
}